@IsTest class AutoConvertLead_Test{
    @isTest static void myUnitTest() {

        // create a Lead
        Lead lead=new Lead(LastName='Doe',FirstName='John',Company='Test',Status='Qualified');

        insert lead;
        // Try to convert lead using the class
        AutoConvertLeads.LeadAssign(new Id[] { lead.Id });
        // Verify lead was converted
        lead = [SELECT IsConverted FROM Lead];
        System.assert(lead.IsConverted, 'Expected lead conversion');
    }
}